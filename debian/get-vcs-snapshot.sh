echo Downloading latest from git repository...
set -e

DEB_SOURCE_PACKAGE=`egrep '^Source: ' debian/control | cut -f 2 -d ' '`
VERSION_UPSTREAM=`dpkg-parsechangelog | grep ^Version: | sed -e 's/^Version:\s*//' -e s/-[^-]*$// -e s/\+git.*//`
VERSION_DATE=`/bin/date --utc +%0Y%0m%0d`
VERSION_FULL="${VERSION_UPSTREAM}+git${VERSION_DATE}"

git clone --depth 1 git://projects.vdr-developer.org/vdr-manager.git

cd vdr-manager
GIT_SHA=`git show --pretty=format:"%h" --quiet | head -1 || true`
cd ..

tar --exclude=.git -czf "../${DEB_SOURCE_PACKAGE}_${VERSION_FULL}.orig.tar.gz" -C vdr-manager vdr-vdrmanager

rm -rf vdr-manager

gbp import-orig --pristine-tar "../${DEB_SOURCE_PACKAGE}_${VERSION_FULL}.orig.tar.gz"
dch -v "$VERSION_FULL-1" "New Upstream Snapshot (commit $GIT_SHA)"
